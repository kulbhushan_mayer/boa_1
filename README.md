#### Download GIT bASH for windows from: https://github.com/git-for-windows/git/releases/download/v2.35.1.windows.2/Git-2.35.1.2-64-bit.exe
#### Install GIT BASH
#### Setup Author Configuration 
```
git config --global user.email "kulbhushan.mayer@gmail.com"
git config --global user.name "Kulbhushan Mayer"
```
#### This data will be update in <user-home-directory>\.gitconfig
#### Initialize the local repository
```
mkdir boa_devops
cd boa_devops/
git init first_repo
cd first_repo/
```
#### `git --help` to get help about git
#### `git command --help` to get information about the particular command like init, clone, commit, etc
#### Stage newly added changes for tracking `git add README.md`
#### Check the status
```
git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   README.md
```
#### Checking history of changes `git log`
```
fatal: your current branch 'master' does not have any commits yet
```
#### Committing the changes `git commit -m "added first file with first change for the day"`
```
[master (root-commit) 3fa9cce] added first file with first change for the day
 1 file changed, 62 insertions(+)
 create mode 100644 README.md
```
#### Check the status `git status`
```
On branch master
nothing to commit, working tree clean
```
#### Checking history of changes `git log`
```
commit 3fa9ccecd510783a99bc60a6490ac24e2dce4246 (HEAD -> master)
Author: Kulbhushan Mayer <kulbhushan.mayer@gmail.com>
Date:   Sun Mar 13 11:19:58 2022 +0530

    added first file with first change for the day
```
#### Get limited output for the histroy `git log --oneline`
```
3fa9cce (HEAD -> master) added first file with first change for the day
```
#### Added few more change to the `README.md`
#### Check the status `git status`
```
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```
#### Stage changes: `git add README.md`
#### Commit changes: `git commit -m "some relevent message"`
#### Add `touch .gitignore` file to the repo, this will help to ensure garbage file, runtime files or any unwanted file will not get added to the repository
#### Check the status `git status`
```
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        .gitignore

no changes added to commit (use "git add" and/or "git commit -a")
```
#### Staging all the changes which include any new addition, any deletion or any modification `git add --all`
#### Check the status `git status`
```
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   .gitignore
        modified:   README.md
```
#### Commiting specific file from the staged changes `git commit -m "adding empty .gitignore file" .gitignore`
```
[master acd789f] adding empty .gitignore file
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 .gitignore
```
#### Check the status `git status`
```
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md
```
#### Added few more changes in the `README.md`
#### Stage the changes and commit the changes with the single command
#### below command is applicable only for modified changes or deletion not for untracked files
`git commit -am "demo for stage and commit in single command"`
```
[master 392fe94] demo for stage and commit in single command
 1 file changed, 47 insertions(+), 1 deletion(-)
```
#### Check the status `git status`
```
On branch master
nothing to commit, working tree clean
```
#### Understanding `.gitignore`
#### Added few files & folders
```
kulbh@thinknyx MINGW64 ~/OneDrive/Documents/code/boa_devops/first_repo (master)
$ touch application.logs application.properties application.tar.gz application_dependency.tar.gz

kulbh@thinknyx MINGW64 ~/OneDrive/Documents/code/boa_devops/first_repo (master)
$ mkdir target

kulbh@thinknyx MINGW64 ~/OneDrive/Documents/code/boa_devops/first_repo (master)
$ touch target/abc.ear target/testresult.html
```
#### Check the status
```
kulbh@thinknyx MINGW64 ~/OneDrive/Documents/code/boa_devops/first_repo (master)
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        application.logs
        application.properties
        application.tar.gz
        application_dependency.tar.gz
        target/

no changes added to commit (use "git add" and/or "git commit -a")
```
#### add `*.tar.gz` in `.gitignore`  # ignore file with specific extension 
#### Check the status
```
kulbh@thinknyx MINGW64 ~/OneDrive/Documents/code/boa_devops/first_repo (master)
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   .gitignore
        modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        application.logs
        application.properties
        target/

no changes added to commit (use "git add" and/or "git commit -a")
```
#### Stage and commit changes upto here
```
git add --all
git commit -m "some message"
```
#### Reverting the code
#### add some changes in the `.gitignore` & `application.properties`
#### Stage & commit the changes `git commit -am "updated for revert demo"`
```
[master 2c5edf9] updated for revert demo
 2 files changed, 5 insertions(+)
```
#### Checking the history `git log --oneline`
```
2c5edf9 (HEAD -> master) updated for revert demo
55a97bb explained .gitignore
392fe94 demo for stage and commit in single command
acd789f adding empty .gitignore file
9fa32dd updated with latest information
3fa9cce added first file with first change for the day
```
#### revert the change `git revert <change_id>`, it will open a file in edit mode to enter the revert commit message, add the message and save the file
```
[master 1fb646f] Revert "updated for revert demo"
 2 files changed, 5 deletions(-)
```
#### Checking the history `git log --oneline`
```
1fb646f (HEAD -> master) Revert "updated for revert demo"
2c5edf9 updated for revert demo
55a97bb explained .gitignore
392fe94 demo for stage and commit in single command
acd789f adding empty .gitignore file
9fa32dd updated with latest information
3fa9cce added first file with first change for the day
```
#### Branching in GIT
#### Checking local branch `git branch`
```
* master
```
#### Checking logs `git log --oneline`
```
0cf462b (HEAD -> master) explained revert
1fb646f Revert "updated for revert demo"
2c5edf9 updated for revert demo
55a97bb explained .gitignore
392fe94 demo for stage and commit in single command
acd789f adding empty .gitignore file
9fa32dd updated with latest information
3fa9cce added first file with first change for the day
```
#### Create new branch from HEAD `git branch b1`
#### Create branch from a specific revision `git branch b2 <revision_id>`
#### Check branches `git branch`, branch with * in front of it is the current branch
```
  b1
  b2
* master
```
#### Checking logs `git log --oneline`
```
0cf462b (HEAD -> master, b1) explained revert
1fb646f Revert "updated for revert demo"
2c5edf9 updated for revert demo
55a97bb (b2) explained .gitignore
392fe94 demo for stage and commit in single command
acd789f adding empty .gitignore file
9fa32dd updated with latest information
3fa9cce added first file with first change for the day
```
#### Chenging branch to b1 `git switch b1`
```Switched to branch 'b1'```
#### Make some changes in any tracked file
#### stage & commit the changes
#### Checking the logs `git log --oneline`
```
26e036d (HEAD -> b1) added changes
0cf462b (master) explained revert
1fb646f Revert "updated for revert demo"
2c5edf9 updated for revert demo
55a97bb (b2) explained .gitignore
392fe94 demo for stage and commit in single command
acd789f adding empty .gitignore file
9fa32dd updated with latest information
3fa9cce added first file with first change for the day
```
#### Deleting the branch `git branch -d b2`, we can't delete the current branch
#### Checking difference between two branches `git diff <branch-1> <branch-2>`
#### Getting the changes from one branch to the another is known as `merge`, current branch should be the one where code needs to be merged `master` in current scenerio i.e. destination, `git merge <source-branch>`, source-branch is the branch from where code needs to be merged
======================================================================================
#### Remote Repositories
#### Create account on https://bitbucket.org by doing a login using `google`
#### Create Remote Repository
#### Checking the remote configured in the local repository `git remote -v`, no output for first time
#### Adding remote repository link in local repository `git remote add origin git@bitbucket.org:kulbhushan_mayer/boa_1.git`
#### Checking the remote repository `git remote -v`
```
origin  git@bitbucket.org:kulbhushan_mayer/boa_1.git (fetch)
origin  git@bitbucket.org:kulbhushan_mayer/boa_1.git (push)
```
#### Generate keypair `ssh-keygen.exe`
```
Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/kulbh/.ssh/id_rsa): ../id_rsa
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in ../id_rsa
Your public key has been saved in ../id_rsa.pub
The key fingerprint is:
SHA256:HGalqBh2Ys0XU+//frb7eUn0FGv8HjlXioVQ4oBO5v4 kulbh@thinknyx
The key's randomart image is:
+---[RSA 3072]----+
|      ooo o..    |
|   o  ++ *..   . |
|  = +=o = o. .. o|
| o = oo+ o  . .=o|
|  . ..  S .  ooo=|
|      .    .. .=+|
|       .    . ..=|
|        E    . .*|
|             .o**|
+----[SHA256]-----+
```
#### Copy the public-key `cat ../id_rsa.pub` and add the same to user personal settings
#### start the ssh-agent
```
eval $(ssh-agent)
ssh-add ../id_rsa
ssh -T git@bitbucket.org
```
#### Push the changes from local to remote repository `git push <alias-name-of-remote> <branch-to-push>`, e.g. `git push origin master`
#### Clone the remote repository from bitbucket.org to local `git clone <repo_url>` e.g. `git clone git@bitbucket.org:kulbhushan_mayer/boa_1.git`
```
Cloning into 'boa_1'...
remote: Enumerating objects: 42, done.
remote: Counting objects: 100% (42/42), done.
Receiving objects: 100% (42/42), 7.33 KiB | 1.05 MiB/s, done.
remote: Compressing objects: 100% (39/39), done.
remote: Total 42 (delta 12), reused 0 (delta 0), pack-reused 0
Resolving deltas: 100% (12/12), done.
```
#### Getting the changes from remote repository pushed by others to the local cloned repository `git pull origin master`
```
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 617 bytes | 18.00 KiB/s, done.
From bitbucket.org:kulbhushan_mayer/boa_1
 * branch            master     -> FETCH_HEAD
   dd22cec..be9acdb  master     -> origin/master
Updating dd22cec..be9acdb
Fast-forward
 README.md | 10 ++++++++++
 1 file changed, 10 insertions(+)
```
